package flow

import (
	"errors"
)

func TeeFloat64(in <-chan float64, outs ...chan<- float64) {
	for input := range in {
		for _, out := range outs {
			out <- input
		}
	}
}

func TeeComplex128(in <-chan complex128, outs ...chan<- complex128) {
	for input := range in {
		for _, out := range outs {
			out <- input
		}
	}
}

func TeeBool(in <-chan bool, outs ...chan<- bool) {
	for input := range in {
		for _, out := range outs {
			out <- input
		}
	}
}

func TeeSliceComplex128(in <-chan []complex128, in_ret chan<- []complex128,
	out1 chan<- []complex128, out1_ret <-chan []complex128,
	out2 chan<- []complex128, out2_ret <-chan []complex128) {
	for input := range in {
		output := <-out1_ret
		if len(output) != len(input) {
			panic(errors.New("Length of slice from output return channel mismatched with slice from input"))
		}
		copy(output, input)
		out1 <- output
		output = <-out2_ret
		if len(output) != len(input) {
			panic(errors.New("Length of slice from output return channel mismatched with slice from input"))
		}
		copy(output, input)
		out2 <- output
		in_ret <- input
	}
}