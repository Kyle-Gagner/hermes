package flow

func AddComplex128(ina <-chan complex128, inb <-chan complex128, out chan<- complex128) {
	for inputa := range ina {
		inputb, ok := <-inb
		if !ok {
			return
		}
		out <- inputa + inputb
	}
}
func SubtractComplex128(ina <-chan complex128, inb <-chan complex128, out chan<- complex128) {
	for inputa := range ina {
		inputb, ok := <-inb
		if !ok {
			return
		}
		out <- inputa - inputb
	}
}