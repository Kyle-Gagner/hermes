package flow

func HardDecideFloat64(in <-chan float64, out chan<- bool) {
	for input := range in {
		out <- input > 0
	}
}