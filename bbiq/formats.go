package bbiq

import (
	"math"
	"math/cmplx"
)

func Qam16() ([]complex128, int) {
	scale := math.Sqrt(2.0 / 5.0)
	symbols, gray := make([]complex128, 16), make(chan int)
	go grayCode(gray, 16)
	row, col := 0, 0
	for row < 4 {
		symbols[<-gray] = complex((float64(col) - 1.5) * scale, (float64(row) - 1.5) * scale)
		if row & 1 == 0 {
			col++
			if col == 4 {
				col--
				row++
			}
		} else {
			col--
			if col == -1 {
				col++
				row++
			}
		}
	}
	return symbols, 4
}

func Bpsk() ([]complex128, int) {
	symbols := make([]complex128, 2)
	symbols[0] = complex(-1, 0)
	symbols[1] = complex(1, 0)
	return symbols, 1
}

func Qpsk() ([]complex128, int) {
	scale := math.Sqrt(2)
	symbols := make([]complex128, 4)
	for i := 0; i < 4; i++ {
		row, col := i >> 1, i & 1
		symbols[i] = complex((float64(col) - 0.5) * scale, (float64(row) - 0.5) * scale)
	}
	return symbols, 2
}

func Npsk(n int) ([]complex128, int) {
	bits := bitsPerSymbol(n)
	symbols, gray := make([]complex128, n), make(chan int)
	go grayCode(gray, n)
	for i := range symbols {
		symbols[<-gray] = cmplx.Exp(complex(float64(0), 2.0 * math.Pi * float64(i) / float64(n)))
	}
	return symbols, bits
}