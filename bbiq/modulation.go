package bbiq

import (
        "math"
	"math/cmplx"
)

func Modulate(in <-chan complex128, out chan<- float64, w float64) {
        t, twopi := float64(0), 2 * math.Pi
        for input := range in {
                out <- real(input * cmplx.Exp(complex(float64(0), t)))
                t += w
                if t > twopi {
                        t -= twopi
                }
        }
        close(out)
}

func Demodulate(in <-chan float64, out chan<- complex128, w float64) {
        t, twopi := float64(0), 2 * math.Pi
        for input := range in {
                out <- complex(input, float64(0)) * cmplx.Exp(complex(float64(0), t))
                t -= w
                if t < 0 {
                        t += twopi
                }
        }
        close(out)
}
