package bbiq

func bitsPerSymbol(count int) int {
	bits := 0
	for (count - 1) >> uint(bits) != 0 {
		bits++
	}
	return bits
}

func grayCode(out chan<- int, count int) {
	gray := 0
	for i := 0; i < count; i++ {
		out <- gray
		flip := 1
		for (i + 1) & flip == 0 {
			flip <<= 1
		}
		gray ^= flip
	}
}