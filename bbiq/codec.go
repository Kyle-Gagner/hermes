package bbiq

import (
	"math"
	"math/cmplx"
)

func MapSymbolsComplex128(in <-chan bool, out chan<- complex128, symbols []complex128) {
	bits := bitsPerSymbol(len(symbols))
	count, index := 0, 0
	for input := range in {
		if input {
			index |= 1 << uint(count)
		}
		count++
		if count == bits {
			if index < len(symbols) {
				out <- symbols[index]
			}
			count, index = 0, 0
		}
	}
}

func SoftDecideComplex128(in <-chan complex128, out chan<- float64, sigma <-chan float64, symbols []complex128) {
	bits := bitsPerSymbol(len(symbols))
	ltrue := make([]float64, bits)
	lfalse := make([]float64, bits)
	for input := range in {
		s, ok := <-sigma
		if !ok {
			break
		}
		for bit := 0; bit < bits; bit++ {
			ltrue[bit] = 0
			lfalse[bit] = 0
		}
		for index, symbol := range symbols {
			likelyhood := math.Exp(-math.Pow(cmplx.Abs(input - symbol), 2) / (2.0 * s * s))
			for bit := 0; bit < bits; bit++ {
				if index & (1 << uint(bit)) == 0 {
					lfalse[bit] += likelyhood
				} else {
					ltrue[bit] += likelyhood
				}
			}
		}
		for bit := 0; bit < bits; bit++ {
			out <- math.Log(ltrue[bit] / lfalse[bit])
		}
	}
}