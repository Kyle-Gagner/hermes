package noise

import (
	"math/rand"
)

func AwgnComplex128(in <-chan complex128, out chan<- complex128, sigma <-chan float64) {
	for input := range in {
		s, ok := <-sigma
		if !ok {
			return
		}
		out <- input + complex(rand.NormFloat64() * s, rand.NormFloat64() * s)
	}
}