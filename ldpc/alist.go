package ldpc

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func LoadAlist(filename string) ([]bool, int, int, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, 0, 0, err
	}
	defer file.Close()
	reader := bufio.NewReader(file)
	line := 1
	var N, M int
	var num_nlist, num_mlist []int
	var biggest_num_n, biggest_num_m int
	var matrix []bool
	var v int
	var text string
	ncount := 0
	mcount := 0
ScanLoop:
	for {
		if line == M + N + 5 {
			break
		}
		text, err = reader.ReadString('\n')
		if err != nil {
			break
		}
		fields := strings.Fields(text)
		if line == 1 {
			if len(fields) < 2 {
				err = errors.New(fmt.Sprintf("alist file line %d: too few tokens", line))
				break
			}
			N, err = strconv.Atoi(fields[0])
			if err != nil || N < 1 {
				err = errors.New(fmt.Sprintf("alist file line %d, token %d: bad value", line, 1))
				break
			}
			M, err = strconv.Atoi(fields[1])
			if err != nil || M < 1 {
				err = errors.New(fmt.Sprintf("alist file line %d, token %d: bad value", line, 2))
				break
			}
			num_nlist = make([]int, N)
			num_mlist = make([]int, M)
			matrix = make([]bool, N * M)
		} else if line == 2 {
			if len(fields) < 2 {
				err = errors.New(fmt.Sprintf("alist file line %d: too few tokens", line))
				break
			}
			biggest_num_n, err = strconv.Atoi(fields[0])
			if err != nil {
				err = errors.New(fmt.Sprintf("alist file line %d, token %d: bad value", line, 1))
				break
			}
			biggest_num_m, err = strconv.Atoi(fields[1])
			if err != nil {
				err = errors.New(fmt.Sprintf("alist file line %d, token %d: bad value", line, 2))
				break
			}
		} else if line == 3 || line == 4 {
			var num_list []int
			if line == 3 {
				num_list = num_nlist
			} else {
				num_list = num_mlist
			}
			if len(fields) < len(num_list) {
				err = errors.New(fmt.Sprintf("alist file line %d: too few tokens", line))
				break
			}
			biggest := 0
			for n := 0; n < len(num_list); n++ {
				v, err = strconv.Atoi(fields[n])
				if err != nil || v < 1 {
					err = errors.New(fmt.Sprintf("alist file line %d, token %d: bad value", line, n + 1))
					break ScanLoop
				}
				num_list[n] = v
				if v > biggest {
					biggest = v
				}
			}
			if line == 3 && biggest != biggest_num_n || line == 4 && biggest != biggest_num_m {
				err = errors.New(fmt.Sprintf("alist file line %d: largest value does not match expected", line))
				break
			}
		} else if line < N + 5 {
			j := line - 5
			if len(fields) < num_nlist[line - 5] {
				err = errors.New(fmt.Sprintf("alist file line %d: too few tokens", line))
				break
			}
			for n := 0; n < num_nlist[line - 5]; n++ {
				v, err = strconv.Atoi(fields[n])
				i := v - 1
				if err != nil || v < 1 || v > M {
					err = errors.New(fmt.Sprintf("alist file line %d, token %d: bad value", line, n + 1))
					break ScanLoop
				}
				matrix[i * N + j] = true
				ncount++
			}
		} else if line < N + M + 5 {
			i := line - N - 5
			if len(fields) < num_mlist[line - N - 5] {
				err = errors.New(fmt.Sprintf("alist file line %d: too few tokens", line))
				break
			}
			for n := 0; n < num_mlist[line - N - 5]; n++ {
				v, err = strconv.Atoi(fields[n])
				j := v - 1
				if err != nil || v < 1 || v > N || !matrix[i * N + j] {
					err = errors.New(fmt.Sprintf("alist file line %d, token %d: bad value", line, n + 1))
					break ScanLoop
				}
				mcount++
			}
		}
		line++
	}
	if err == nil && mcount != ncount {
		err = errors.New("alist file: length of nlist and mlist mismatched")
	}
	if err == nil {
		return matrix, M, N, nil
	} else {
		return nil, 0, 0, err
	}
}

func SaveAlist(matrix []bool, rows int, cols int, filename string) error {
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()
	writer := bufio.NewWriter(file)
	N, M := cols, rows
	num_nlist := make([]int, N)
	num_mlist := make([]int, M)
	biggest_num_n, biggest_num_m := 0, 0
	for j := 0; j < N; j++ {
		count := 0
		for i := 0; i < M; i++ {
			if matrix[i * N + j] {
				count++
			}
		}
		num_nlist[j] = count
		if count > biggest_num_n {
			biggest_num_n = count
		}
	}
	for i := 0; i < M; i++ {
		count := 0
		for j := 0; j < N; j++ {
			if matrix[i * N + j] {
				count++
			}
		}
		num_mlist[i] = count
		if count > biggest_num_m {
			biggest_num_m = count
		}
	}
	writer.WriteString(fmt.Sprintf("%d %d\n", N, M))
	writer.WriteString(fmt.Sprintf("%d %d\n", biggest_num_n, biggest_num_m))
	for j := 0; j < N; j++ {
		writer.WriteString(fmt.Sprintf("%d ", num_nlist[j]))
	}
	writer.WriteString("\n")
	for i := 0; i < M; i++ {
		writer.WriteString(fmt.Sprintf("%d ", num_mlist[i]))
	}
	writer.WriteString("\n")
	for j := 0; j < N; j++ {
		count := 0
		for i := 0; i < M; i++ {
			if matrix[i * N + j] {
				writer.WriteString(fmt.Sprintf("%d ", i + 1))
				count++
			}
		}
		for count < biggest_num_n {
			writer.WriteString("0 ")
			count++
		}
		writer.WriteString("\n")
	}
	for i := 0; i < M; i++ {
		count := 0
		for j := 0; j < N; j++ {
			if matrix[i * N + j] {
				writer.WriteString(fmt.Sprintf("%d ", j + 1))
				count++
			}
		}
		for count < biggest_num_m {
			writer.WriteString("0 ")
			count++
		}
		writer.WriteString("\n")
	}
	writer.Flush()
	return nil
}