package ldpc

import (
	"math"
)

func ParityToGenerator(parity []bool, prows int, pcols int) ([]bool, []bool, int, int) {
	// copy parity matrix and use elementary row operations to transform to row echelon form
	echelon := make([]bool, prows * pcols)
	copy(echelon[:], parity)
	// dim will be the number of nonzero rows after the transformation (dimension of the rowspace)
	var dim int
	// dbits will contain true for each column which represents a data bit (does not contain a leading coefficient)
	dbits := make([]bool, pcols)
	for pcol := range dbits {
		dbits[pcol] = true
	}
	// loop across rows of the matrix (erow)
	for erow := 0; erow < prows; erow++ {
		// column major search (scol, srow) the submatrix below erow for first true
SearchLoop:
		for scol := 0; scol < pcols; scol++ {
			for srow := erow; srow < prows; srow++ {
				if echelon[srow * pcols + scol] {
					// with the srow containing the leading coefficient at scol found, swap with erow
					if srow != erow {
						for j := 0; j < pcols; j++ {
							u, v := &echelon[srow * pcols + j], &echelon[erow * pcols + j]
							*u, *v = *v, *u
						}
					}
					// clear coefficients in scol by adding row at erow into other rows as necessary
					for i := 0; i < prows; i++ {
						if i == erow {
							continue
						}
						if echelon[i * pcols + scol] {
							for j := 0; j < pcols; j++ {
								u := &echelon[i * pcols + j]
								// != implements addition (XOR)
								*u = *u != echelon[erow * pcols + j]
							}
						}
					}
					// update dbits and dim and terminate the search loop
					dbits[scol] = false
					dim = erow + 1
					break SearchLoop
				}
			}
		}
	}
	// create generator matrix
	grows := pcols - dim
	gcols := pcols
	generator := make([]bool, grows * gcols)
	// construct the generatow by columns (gcol), 
	for gcol, pbit, dbit := 0, 0, 0; gcol < gcols; gcol++ {
		// the column may generate a parity bit or a data bit, find out which
		if dbits[gcol] {
			// create data bit in row dbit
			generator[dbit * gcols + gcol] = true
			dbit++
		} else {
			// use constraints on the parity bit in echelon row pbit to create the generator row
			for grow, pcol := 0, 0; grow < grows; grow++ {
				for !dbits[pcol] {
					pcol++
				}
				generator[grow * gcols + gcol] = echelon[pbit * pcols + pcol]
				pcol++
			}
			pbit++
		}
	}
	// return the result
	return generator, dbits, grows, gcols
}

func Encode(in <-chan bool, out chan<- bool, parity []bool, rows int, cols int) {
	generator, _, grows, gcols := ParityToGenerator(parity, rows, cols)
	data, count := make([]bool, grows), 0
	for input := range in {
		data[count] = input
		count++
		if count == grows {
			for j := 0; j < gcols; j++ {
				output := false
				for i := 0; i < grows; i++ {
					output = output != (data[i] && generator[i * gcols + j])
				}
				out <- output
			}
			count = 0
		}
	}
}

func pingPong(in <-chan float64, out chan<- float64, channels []chan float64, iterations int) {
	inbox := make([]float64, len(channels))
	for input := range in {
		for _, channel := range channels {
			channel <- input
		}
		for iteration := 0; iteration < iterations - 1; iteration++ {
			for fromind, fromchan := range channels {
				inbox[fromind] = <-fromchan
			}
			for toind, tochan := range channels {
				ping := input
				for fromind := range channels {
					if fromind != toind {
						ping += inbox[fromind]
					}
				}
				tochan <- ping
			}
		}
		output := input
		for _, fromchan := range channels {
			output += <-fromchan
		}
		out <- output
	}
}

func pongPing(channels []chan float64) {
	inbox := make([]float64, len(channels))
	for {
		for fromind, fromchan := range channels {
			ping, ok := <-fromchan
			if !ok {
				return
			}
			inbox[fromind] = math.Tanh(ping / 2.0)
		}
		for toind, tochan := range channels {
			pong := 1.0
			for fromind := range channels {
				if fromind != toind {
					pong *= inbox[fromind]
				}
			}
			tochan <- math.Atan(pong) * 2.0
		}
	}
}

func DecodeFloat64(in <-chan float64, out chan<- float64, parity []bool, rows int, cols int, iterations int) {
	_, dbits, _, _ := ParityToGenerator(parity, rows, cols)
	pingchans := make([][]chan float64, cols)
	pongchans := make([][]chan float64, rows)
	inchans := make([]chan float64, cols)
	outchans := make([]chan float64, cols)
	for j := 0; j < cols; j++ {
		pingchans[j] = make([]chan float64, 0, rows)
		inchans[j] = make(chan float64)
		outchans[j] = make(chan float64)
	}
	for i := 0; i < rows; i++ {
		pongchans[i] = make([]chan float64, 0, cols)
	}
	for j := 0; j < cols; j++ {
		for i := 0; i < rows; i++ {
			if parity[i * cols + j] {
				channel := make(chan float64)
				pingchans[j] = append(pingchans[j], channel)
				pongchans[i] = append(pongchans[i], channel)
			}
		}
	}
	for j := 0; j < cols; j++ {
		go pingPong(inchans[j], outchans[j], pingchans[j], iterations)
	}
	for i := 0; i < rows; i++ {
		go pongPing(pongchans[i])
	}
	count := 0
	for input := range in {
		inchans[count] <- input
		count++
		if count == cols {
			for j := 0; j < cols; j++ {
				output := <-outchans[j]
				if dbits[j] {
					out <- output
				}
			}
			count = 0
		}
	}
	for _, channels := range pingchans {
		for _, channel := range channels {
			close(channel)
		}
	}
}