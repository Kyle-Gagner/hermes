package fir

func ConvolveComplex128(in <-chan complex128, out chan<- complex128, taps []complex128) {
	buffer := make([]complex128, len(taps))
	n := 0
	for input := range in {
		buffer[n] = input
		var y complex128
		for i, tap := range taps {
			y += tap * buffer[(n - i + len(taps)) % len(taps)]
		}
		out <- y
		n++
		if n == len(taps) {
			n = 0
		}
	}
}

func InterpolateComplex128(in <-chan complex128, out chan<- complex128, response []float64, upsample int) {
	buffer := make(chan complex128, len(response))
	for i := 0; i < cap(buffer) - upsample; i++ {
		buffer <- 0.0
	}
	for u := range in {
		for i := 0; i < upsample; i++ {
			buffer <- 0.0
		}
		for _, v := range response {
			buffer <- complex(v, float64(0)) * u + <-buffer
		}
		for i := 0; i < upsample; i++ {
			out <- <-buffer
		}
	}
	for u := range buffer {
		out <- u
	}
	close(out)
}

func DecimateComplex128(in <-chan complex128, out chan<- complex128, response []float64, downsample int) {
	temp := make([]float64, downsample * ((len(response) + downsample - 1) / downsample))
	copy(temp, response)
	response = temp
	buffer := make([]complex128, len(response) / downsample)
	offset := 0
	rotate := 0
	for u := range in {
		for i := 0; i < len(buffer); i++ {
			buffer[(i + rotate) % len(buffer)] += complex(response[i * downsample + offset], 0) * u
		}
		if offset == 0 {
			out <- buffer[rotate]
			buffer[rotate] = 0
			offset = downsample
			rotate++
			if rotate == len(buffer) {
				rotate = 0
			}
		}
		offset--
	}
	for i := 0; i < len(buffer); i++ {
		out <- buffer[(i + rotate) % len(buffer)]
	}
	close(out)
}
