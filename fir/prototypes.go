package fir

import (
	"math"
)

func RrcPrototype(beta float64, period float64, samples int, oversample int) []float64 {
        midpoint := samples
        result := make([]float64, 2 * midpoint + 1)
        result[midpoint] = (1.0 + beta * (4.0 / math.Pi - 1.0)) / period
        for i := 1; i <= midpoint; i++ {
                t := float64(i) / float64(oversample);
                denominator := (math.Pi * t / period) * (1 - math.Pow(4 * beta * t / period, 2))
                var value float64
                if denominator == 0 {
                        value = (beta / (period * math.Sqrt(2))) * ((1.0 + 2.0 / math.Pi) * math.Sin(math.Pi / (4.0 * beta)) +
                                (1.0 - 2.0 / math.Pi) * math.Cos(math.Pi / (4.0 * beta)))
                } else {
                        value = (math.Sin(math.Pi * t * (1.0 - beta) / period) +
                                4 * beta * t * math.Cos(math.Pi * t * (1.0 + beta) / period) / period) / (denominator * period)
                }
                result[midpoint + i] = value
                result[midpoint - i] = value
        }
        return result
}
