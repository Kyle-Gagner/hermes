package main

import (
	"encoding/json"
	"fmt"
	"math"
	"math/rand"
	"os"
	"gitlab.com/Kyle-Gagner/hermes/bbiq"
	"gitlab.com/Kyle-Gagner/hermes/flow"
	"gitlab.com/Kyle-Gagner/hermes/ldpc"
	"gitlab.com/Kyle-Gagner/hermes/noise"
)

type datapoint struct {
	modulation string
	ldpc bool
	snr float64
	ber float64
}

func (d datapoint) MarshalJSON() ([]byte, error) {
	var err_corr string
	if d.ldpc {
		err_corr = "With LDPC"
	} else {
		err_corr = "No LDPC"
	}
	return json.Marshal(map[string]interface{}{
		"Modulation": d.modulation,
		"Error Correction": err_corr,
		"SNR": d.snr,
		"BER": d.ber})
}

func lcm(a int, b int) int {
	min, max := 0, 0
	if (a < b) {
		min, max = a, b
	} else {
		min, max = b, a
	}
	n := min
	for n % max != 0 {
		n += min
	}
	return n
}

func experiment(
	start float64, stop float64, step float64,
	parity []bool, rows int, cols int,
	symbols []complex128, bits int,
	minerrs int, maxbits int,
	modulation string, dataset []datapoint) []datapoint {
	// the experiment may be run with or without LDPC
	do_ldpc := parity != nil
	grows, gcols := 1, 1
	if do_ldpc {
		_, _, grows, gcols = ldpc.ParityToGenerator(parity, rows, cols)
	}
	// bit_block is the number of input bits which will be statistically correlated after passing to the output
	bit_block := lcm(bits, grows)
	// sym_block is the number of symbols used to send bit_block number of bits across the noisy channel
	sym_block := bit_block * gcols / bits / grows

	// sigma, the noise level, is split across two data paths, sigma1 and sigma2
	sigma := make(chan float64, sym_block)
	sigma1, sigma2 := make(chan float64), make(chan float64)
	go flow.TeeFloat64(sigma, sigma1, sigma2)
	// input data is fed in this channel and also split across two data paths, one for reference and one experimental
	input := make(chan bool, bit_block)
	experimental, reference := make(chan bool), make(chan bool, bit_block)
	go flow.TeeBool(input, experimental, reference)
	// if using LDPC, encoded_bits is the LDPC encoded data, otherwise input1 passes directly through as encoded_bits
	var encoded_bits chan bool
	if do_ldpc {
		encoded_bits = make(chan bool)
		go ldpc.Encode(experimental, encoded_bits, parity, rows, cols)
	} else {
		encoded_bits = experimental
	}
	// the encoded_bits are mapped to symbols
	raw_symbols := make(chan complex128)
	go bbiq.MapSymbolsComplex128(encoded_bits, raw_symbols, symbols)
	// the raw_symbols are corrupted with noise (noise level fed in from sigma1)
	noisy_symbols := make(chan complex128)
	go noise.AwgnComplex128(raw_symbols, noisy_symbols, sigma1)
	// the noisy_symbols are unmapped back to bits with a degree of confidence depending on the noise level (sigma2)
	soft_bits := make(chan float64)
	go bbiq.SoftDecideComplex128(noisy_symbols, soft_bits, sigma2, symbols)
	var decoded_bits chan float64
	// if using LDPC, soft_bits is the LDPC decoded data, otherwise soft_bits passes directly through as decoded_bits
	if do_ldpc {
		decoded_bits = make(chan float64)
		go ldpc.DecodeFloat64(soft_bits, decoded_bits, parity, rows, cols, 10)
	} else {
		decoded_bits = soft_bits
	}
	// a hard (and possibly mistaken, due to noise) decision is made on the value of each bit
	output := make(chan bool, bit_block)
	go flow.HardDecideFloat64(decoded_bits, output)

	// run experiments at different signal to noise ratios (SNR) to plot a trend of bit error rate (BER) versus SNR
	for snr := start; snr < stop + 0.0001; snr += step {
		// determine sigma from SNR which is in logarithmic units
		s := math.Sqrt(math.Exp(-(snr / 10.0) * math.Log(10)))
		// number of trials each involving a number (bit_block) observations and number of bit errors observed
		trials, errors := 0, 0
		// stop after a statistically significant number of errors (minerrs) or large number of observations (maxbits)
		// it is important to run at least two trials to avoid problems with the standard deviation calculation later
		for trials < 2 || errors < minerrs && trials * bit_block < maxbits {
			for n := 0; n < bit_block; n++ {
				input <- rand.Int() & 4 != 0
			}
			for n := 0; n < sym_block; n++ {
				sigma <- s
			}
			x := 0
			for n := 0; n < bit_block; n++ {
				if <-output != <-reference {
					x++
				}
			}
			errors += x
			trials++
		}
		// calculate the bit error rate
		ber := float64(errors) / float64(trials * bit_block)
		if ber == 0 {
			ber = math.SmallestNonzeroFloat32
		}
		// append the datapoint to the dataset
		dataset = append(dataset, datapoint{modulation, do_ldpc, snr, ber})
	}
	// return the modified dataset
	return dataset
}

func main() {
	parity, rows, cols, err := ldpc.LoadAlist("gallager.alist")
	if err != nil {
		panic(err)
	}

	var symbols []complex128
	var bits int
	var modulation string
	minerrs := 1000
	maxtrials := 1000000
	start, stop, step := 0.0, 15.0, 0.5
	
	fmt.Println("Working...")

	dataset := make([]datapoint, 0)

	for n := 0; n < 4; n++ {
		switch (n) {
		case 0:
			symbols, bits = bbiq.Bpsk()
			modulation = "BPSK"
		case 1:
			symbols, bits = bbiq.Qpsk()
			modulation = "QPSK"
		case 2:
			symbols, bits = bbiq.Npsk(8)
			modulation = "8-PSK"
		case 3:
			symbols, bits = bbiq.Qam16()
			modulation = "16-QAM"
		}
		dataset = experiment(
			start, stop, step,
			nil, 0, 0,
			symbols, bits,
			minerrs, maxtrials,
			modulation, dataset)
		dataset = experiment(
			start, stop, step,
			parity, rows, cols,
			symbols, bits,
			minerrs, maxtrials,
			modulation, dataset)
		fmt.Printf("%d%%\n", 25 * (n + 1))
	}

	// create a file to store the dataset
	file, err := os.Create("data.json")
	if err != nil {
		panic(err)
	}
	defer file.Close()
	
	// dump the dataset using a JSON encoder
	encoder := json.NewEncoder(file)
	encoder.SetIndent("", "    ")
	encoder.Encode(dataset)
}