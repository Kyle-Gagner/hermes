import json
import numpy as np
import matplotlib.pyplot as plt

modulations = ['BPSK', 'QPSK', '8-PSK', '16-QAM']

with open('data.json') as f:
    dataset = json.load(f)

fig = plt.figure(figsize=(10.5, 8))
for n in range(len(modulations)):
    plt.subplot(2, 2, n + 1)
    modulation = modulations[n]
    for series in ('No LDPC', 'With LDPC'):
        filtered = [d for d in dataset if d['Modulation'] == modulation and d['Error Correction'] == series]
        snr = np.array([d['SNR'] for d in filtered])
        ber = np.array([d['BER'] for d in filtered])
        plt.plot(snr, ber, label=series)
        plt.xlabel('SNR (dB)')
        plt.ylabel('BER')
        plt.legend()
        plt.yscale('log')
        plt.ylim((1e-4, 0.5))
        plt.xticks(range(0, 16))
        plt.grid(b=True, which='both')
        plt.title("BER vs SNR " + modulation)
plt.tight_layout()
fig.savefig('results.png', bbox_inches='tight')