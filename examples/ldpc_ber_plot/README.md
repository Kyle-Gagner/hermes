This example plots bit error rate (BER) versus signal to noise ratio (SNR) for BPSK, QPSK, 8-PSK, and 16-QAM with and without the use of a low density parity check error correcting code.

```
go run main.go
python3 plot.py
```

![BER vs SNR plots](https://i.imgur.com/ejFGFzx.png)

Parity check matrix from:

R. Gallager, "Low-density parity-check codes," in IRE Transactions on Information Theory, vol. 8, no. 1, pp. 21-28, January 1962.

doi: 10.1109/TIT.1962.1057683