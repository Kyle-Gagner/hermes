package main

import (
	"encoding/json"
	"math"
	"math/cmplx"
	"math/rand"
	"os"
	"gitlab.com/Kyle-Gagner/hermes/adapt"
	"gitlab.com/Kyle-Gagner/hermes/fir"
	"gitlab.com/Kyle-Gagner/hermes/flow"
	"gitlab.com/Kyle-Gagner/hermes/noise"
)

func main() {
	// parameters to set up the simulation
	fir_length := 10
	nlms_length := 40
	nlms_mu := 0.1
	nlms_gamma := 1e-12
	snr := 20.0
	delay := 20
	training_length := 10000
	response_length := fir_length + nlms_length - 1

	// create the fir filter taps as noise under an exponentially decreasing envelope
	fir_taps := make([]complex128, fir_length)
	for i := range fir_taps {
		fir_taps[i] = complex(rand.NormFloat64(), rand.NormFloat64()) *
			complex(math.Pow(0.1, -float64(i) / float64(fir_length - 1)), 0)
	}

	// determine sigma of the awgn source by power relative to the total energy of the fir filter
	p := float64(0)
	for _, tap := range fir_taps {
		p += real(cmplx.Conj(tap) * tap)
	}
	sigma := math.Pow(0.1, snr / 10.0) * p

	// make the channels to connect the dataflow
	input := make(chan complex128, 1)
	fir_in := make(chan complex128)
	fir_out := make(chan complex128)
	awgn_sigma := make(chan float64, 1)
	awgn_out := make(chan complex128)
	nlms_x := make(chan complex128)
	nlms_r := make(chan complex128)
	nlms_e := make(chan complex128, 1)
	nlms_y := make(chan complex128)
	desired := make(chan complex128, delay)
	feedback := make(chan complex128)
	output := make(chan complex128, 1)
	
	// incur a delay between input samples coming in and samples coming out of desired
	for i := 0; i < delay; i++ {
		desired <- complex(0, 0)
	}

	// construct the entire dataflow
	go flow.TeeComplex128(input, fir_in, desired)
	go fir.ConvolveComplex128(fir_in, fir_out, fir_taps)
	go noise.AwgnComplex128(fir_out, awgn_out, awgn_sigma)
	go flow.TeeComplex128(awgn_out, nlms_x, nlms_r)
	go adapt.NlmsComplex128(nlms_x, nlms_r, nlms_e, nlms_y, nlms_length, nlms_mu, nlms_gamma)
	go flow.TeeComplex128(nlms_y, feedback, output)
	go flow.SubtractComplex128(desired, feedback, nlms_e)

	// set up the training / measurement loop
	response := make([]complex128, response_length)
	n := -training_length - response_length
	// loop
	for {
		// train the NLMS filter for training_length
		if n < -response_length {
			input <- complex(rand.NormFloat64(), rand.NormFloat64())
			awgn_sigma <- sigma
			<-output
		// settle the system before measurement
		} else if n < 0 {
			input <- complex(0, 0)
			awgn_sigma <- 0.0
			<-output
		// measure the impulse response of the system as a whole
		} else if n < response_length {
			if n == 0 {
				input <- complex(1, 0)
			} else {
				input <- complex(0, 0)
			}
			awgn_sigma <- 0.0
			response[n] = <-output
		} else {
			break
		}
		n++
	}

	// create a file to store the dataset
	file, err := os.Create("data.json")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	// create the dataset
	dataset := make([]map[string]float64, 0)
	for i, value := range response {
		dataset = append(dataset, map[string]float64{"x": float64(i), "y": real(cmplx.Conj(value) * value)})
	}

	// dump the dataset using a JSON encoder
	encoder := json.NewEncoder(file)
	encoder.SetIndent("", "    ")
	encoder.Encode(dataset)
}