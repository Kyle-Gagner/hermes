import json
import numpy as np
import matplotlib.pyplot as plt

modulations = ['BPSK', 'QPSK', '8-PSK', '16-QAM']

with open('data.json') as f:
    dataset = json.load(f)

fig = plt.figure(figsize=(10.5, 8))
plt.plot([d['x'] for d in dataset], [d['y'] for d in dataset], 'o')
plt.yscale('log')
plt.xlabel("Time (samples)")
plt.ylabel("Power")
plt.grid(b=True, which='both')
plt.title("Impulse Response: Power versus Time")
plt.tight_layout()
fig.savefig('results.png', bbox_inches='tight')