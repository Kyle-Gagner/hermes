This example shows an NLMS filter used as a channel equalizer. A random signal at the input of the signal processing
dataflow is split into two paths. Along one path it encounters signal impairments: a channel response which smears
the signal out in time and noise added to the signal. Along the other path, it is delayed some amount of time, then
used as a reference for an NLMS filter. The NLMS filter's job is to correct the channel response to the greatest extent
possible. It cannot, however, remove the noise which was added. After many samples of random data are used to train the
filter, the noise impairment is disabled and a single impulse is sent through the dataflow. The NLMS filter has
approximately equalized the channel, so although it is impaired by the channel response, the smeared out impulse is
corrected by the NLMS filter and a delayed impulse is reconstructed.

![](https://i.imgur.com/T7Ft4YD.png)