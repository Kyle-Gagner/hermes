package adapt

import (
	"errors"
	"math/cmplx"
)

func NlmsComplex128(x <-chan complex128, r <-chan complex128, e <-chan complex128, y chan<- complex128,
	length int, mu float64, gamma float64) {
	taps := make([]complex128, length)
	x_vector := make([]complex128, length)
	r_vector := make([]complex128, length)
	n := 0
	for {
		var ok bool
		x_vector[n], ok = <-x
		if !ok {
			break
		}
		var v complex128
		for i := 0; i < length; i++ {
			v += cmplx.Conj(taps[i]) * x_vector[(n - i + length) % length]
		}
		y <- v
		r_vector[n], ok = <-r
		if !ok {
			break
		}
		var s float64
		for i := 0; i < length; i++ {
			s += real(cmplx.Conj(r_vector[i]) * r_vector[i])
		}
		e_value, ok := <-e
		if !ok {
			break
		}
		for i := 0; i < length; i++ {
			taps[i] += complex(mu / (gamma + s), 0) * cmplx.Conj(e_value) * r_vector[(n - i + length) % length]
		}
		n++
		if n == length {
			n = 0
		}
	}
}

func AdaptiveFirComplex128(x <-chan complex128, y chan<- complex128,
	taps <-chan []complex128, ret chan<- []complex128, length int) {
	x_vector := make([]complex128, length)
	n := 0
	for {
		var ok bool
		x_vector[n], ok = <-x
		if !ok {
			break
		}
		var c []complex128
		c, ok = <-taps
		if !ok {
			break
		}
		if len(c) != length {
			panic(errors.New("Unexpected length of slice from taps channel"))
		}
		var v complex128
		for i := 0; i < length; i++ {
			v += cmplx.Conj(c[i]) * x_vector[(n - i + length) % length]
		}
		y <- v
		ret <- c
		n++
		if n == length {
			n = 0
		}
	}
}

func NlmsRegressionComplex128(r <-chan complex128, e <-chan complex128,
	taps chan<- []complex128, ret <-chan []complex128, length int, mu float64, gamma float64) {
	coeffs := make([]complex128, length)
	r_vector := make([]complex128, length)
	n := 0
	for {
		var ok bool
		r_vector[n], ok = <-r
		if !ok {
			break
		}
		var s float64
		for i := 0; i < length; i++ {
			s += real(cmplx.Conj(r_vector[i]) * r_vector[i])
		}
		e_value, ok := <-e
		if !ok {
			break
		}
		for i := 0; i < length; i++ {
			coeffs[i] += complex(mu / (gamma + s), 0) * cmplx.Conj(e_value) * r_vector[(n - i + length) % length]
		}
		c := <-ret
		copy(c, coeffs)
		taps <- c
		n++
		if n == length {
			n = 0
		}
	}
}