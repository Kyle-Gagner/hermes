# Hermes

Hermes is a collection of Go packages for signal processing dataflows. Hermes prioritizes ease of leverage and
simplicity of implementation over raw efficiency. The goal is to make signal processing more fun and more accessible.

## Packages
  - adapt
    - Adaptive filters are a special kind of filter that self optimize their transfer function to achieve a desired
    output given some input and an additional source of information which indicates the difference between the desired
    and actual outputs.
    - Package implements Normalized Least Mean Squares Filter (NLMS). NLMS filters do not have the fastest convergence
    characteristics or best efficiency, but they are simple to use and simple to understand. Unlike its un-normalized
    counterpart, the LMS filter, the NLMS filter is insensitive to scaling of its input. The NLMS filter is a simple
    and robust choice for a variety of applications.
  - bbiq
    - Baseband In-phase / Quadrature signals describe the magnitude and phase of a signal in cartesian form as the sum
    of signal components in-phase and 90° out of phase (quadrature phase).
    - Package implements an encoder from bit streams to BBIQ symbol streams and a corresponding soft-output decoder.
    - Package also implements utilities to construct constellations for multiple modulation schemes including:
      - BPSK - binary phase shift keying
      - QPSK - quadrature phase shift keying
      - NPSK - phase shift keying for any (power of two) number of phases
      - QAM16 - 16 state quadrature amplitude modulation
  - fir
    - Finite Impulse Response filters are an essential component of most signal processing applications as they are
    relatively straightforward to design and analyze.
  - flow
    - The flow package is a collection of miscellaneous components which aid in connecting complex dataflows or adapting
    between different types of inputs and outputs.
  - ldpc
    - Low Density Parity Check is a class of error correcting codes
    - Package implements an LDPC encoder, a SISO (soft-input, soft-output) belief propagation decoder, and utilities
    used to construct the matrices which define LDPC codes.
  - noise
    - The noise package contains noise generation components which can be used to generate noise signals or simulate
    noisy channel signal impairments.